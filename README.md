# Public Repository Search
One endpoint to search through multiple git hosting services, like Gitlab, Gitlub and Bitbucket.

### Requirements

- Python3.8
- virtualenv and virtualenvwrapper

### Setup development environment
Clone the git repository
```
$ git clone git@gitlab.com:timmuller/public-repository-search.git public_repository_search
cd public_repository_search
```
And setup the virtualenv environment
```
$ mkvirtualenv  -a $(pwd) -ppython3.8 public_repository_search
$ pip install -r requirements/development.txt
$ ./manage.py migrate
```

### Run development server
Start the webapplication, that serves the API to search through repositories:
```
$ ./manage.py runserver
```
This will start the webservice listing to http://localhost:8000/ .
The service will store data into the localstorage sqlite3 database.

### Running tests
To run the tests of this framework, the script `runtests.sh` can be triggered.
This will run the testsuite of django, and checks for linting errors.
```
$ ./runtests.sh
```

### Linting
This project uses flake8 to lint the python code.
It uses the default configuratoin with one exception, namely the max characters on one line.
The characters allowed on one line are set to 120 for this project, instead of the default 79.

## Documentation

### How to use API
__List repositories on gitlab__  
http://localhost:8000/api/repository/

__List repositories on gitlab explicitly__  
http://localhost:8000/api/repository/?provider=gitlab

__List repositories on github__  
http://localhost:8000/api/repository/?provider=github

__Search repositories on name and description__  
http://localhost:8000/api/repository/?search=searchterm

__Search repositories on name and description on github__  
http://localhost:8000/api/repository/?search=searchterm&provider=github

### What is archieved
- Able to list repositories from gitlab
- Able to search on name and description on gitlab
- Able to list repositories from github
- Able to search github repositories on name and description
- Switch between git-provider in query string parameters


### Improvements
- CI/CD to validate correctness of project on each commit push
- Split settings into different environment configuration (development, production)
- API versioning
- Document query string parameters in swagger documentation
- Pagination of API, currently only shows first 20 results for gitlab, and 100 results of github
- Fetch first and lastname from author on gitlab integration; second call for user information is required

### Lessons learned

__Lesson 1; Django Rest Framework on API integrations instead of ORM__  
I'm familiar with Django REST Framework (DRF), based on an ORM.
In this project I found out DRF can also be used on API integrations, when one uses a DTO to use within the serializers.
Learned this before starting the project while orientating how to setup.

__Lesson 2; Author Information__
Basic information of an author isn't always available when expected.
Gitlab always returns firstname and lastname of the author, in which Github requires a second call

### Difficulties encountered

__GitHub search-result differs from list endpoint__  
GitHub list repositories directly within a list, while the search endpoint encapsulates the results into an `items` key which needs to be unpacked separatly
