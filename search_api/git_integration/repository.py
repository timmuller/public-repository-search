from json.decoder import JSONDecodeError

import requests
from requests import HTTPError

PROVIDER_GITLAB = 'gitlab'
PROVIDER_GITHUB = 'github'


class APIError(Exception):
    pass


class Repository(object):
    def __init__(self, name, description, link, clone_url, author, author_username):
        self.name = name
        self.description = description
        self.link = link
        self.clone_url = clone_url
        self.author = author
        self.author_username = author_username


def get_repositories(provider=PROVIDER_GITLAB, search=None):
    provider_class = get_provider(provider)
    if not provider_class:
        raise APIError("Provider {} does not exist".format(provider))

    if not search:
        return provider_class().list_public_repositories()
    return provider_class().search_public_repositories(search)


def get_provider(provider):
    if provider in (PROVIDER_GITLAB, None):
        return GitLabProvider
    if provider == PROVIDER_GITHUB:
        return GitHubProvider


class ProviderBase:
    @classmethod
    def search_public_repositories(cls, search):
        raise NotImplementedError()

    @classmethod
    def list_public_repositories(cls):
        raise NotImplementedError()

    @classmethod
    def invoke_api(cls, url, params):
        ret = requests.get(url, params=params)
        try:
            ret.raise_for_status()
            return ret.json()
        except HTTPError as e:
            raise APIError("Communication Error: {}".format(e))
        except JSONDecodeError:
            raise APIError("Communication Error: No valid JSON content returned")


class GitHubProvider(ProviderBase):
    API_URL = 'https://api.github.com/'

    @classmethod
    def list_public_repositories(cls):
        repository_data = cls.invoke_api(cls.API_URL + "repositories", params={})
        return [cls.convert_to_repository(repository) for repository in repository_data]

    @classmethod
    def search_public_repositories(cls, search):
        search_results_json = cls.invoke_api(cls.API_URL + "search/repositories", params={'q': search})
        repository_data = search_results_json['items']
        return [cls.convert_to_repository(repository) for repository in repository_data]

    @classmethod
    def convert_to_repository(cls, repository_data):
        return Repository(
            name=repository_data['name'],
            description=repository_data['description'],
            link=repository_data['html_url'],
            clone_url=repository_data['html_url'] + ".git",
            author='',
            author_username=repository_data['owner']['login']
        )


class GitLabProvider(ProviderBase):
    API_URL = 'https://gitlab.com/api/v4/projects'

    @classmethod
    def search_public_repositories(cls, search):
        return cls._integration(search)

    @classmethod
    def list_public_repositories(cls):
        return cls._integration()

    @classmethod
    def _integration(cls, search=None):
        params = {}
        if search:
            params.update({'search': search})
        repository_data = cls.invoke_api(cls.API_URL, params)

        return [Repository(
            name=repository['name'],
            description=repository['description'],
            link=repository['web_url'],
            clone_url=repository['ssh_url_to_repo'],
            author=repository['namespace']['name'],
            author_username=repository['namespace']['path'],

        ) for repository in repository_data]
