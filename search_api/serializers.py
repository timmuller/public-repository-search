from rest_framework import serializers


class RepositorySerializer(serializers.Serializer):
    name = serializers.CharField()
    description = serializers.CharField()
    link = serializers.CharField()
    clone_url = serializers.CharField()
    author = serializers.CharField()
    username = serializers.CharField(source='author_username')
