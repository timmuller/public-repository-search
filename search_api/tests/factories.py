import factory
from search_api.git_integration.repository import Repository


class RepositoryFactory(factory.Factory):
    class Meta:
        model = Repository

    name = factory.Sequence(lambda x: "Repository {}".format(x))
    description = 'description'
    link = factory.Sequence(lambda x: "https://example/{}".format(x))
    clone_url = factory.Sequence(lambda x: "git://example/{}".format(x))
    author = "First Lastname"
    author_username = "username"
