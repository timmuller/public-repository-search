from search_api.git_integration.repository import get_repositories, PROVIDER_GITHUB, Repository, APIError
from search_api.tests.git_integration.testcase import IntegrationTestCase, StubResponse


class TestGitHubListRepositories(IntegrationTestCase):
    def setUp(self):
        self.mock_requests = self.setup_mock('search_api.git_integration.repository.requests')
        self.mock_requests.get.return_value = self.get_github_fixture_response()

    def get_github_fixture_response(self):
        return self.get_fixture_response('github_repositories_list.json')

    def get_github_search_fixture_response(self):
        return self.get_fixture_response('github_search_list.json')

    def get_repositories(self, search=None):
        return get_repositories(provider=PROVIDER_GITHUB, search=search)

    def test_that_repositories_from_fixture_are_returned(self):
        repositories = self.get_repositories()
        self.assertEqual(len(repositories), 100)

    def test_that_return_value_is_list_of_repository_responses(self):
        repositories = self.get_repositories()
        all_repository_objects = (isinstance(repository, Repository) for repository in repositories)
        self.assertTrue(all(all_repository_objects))

    def test_that_values_are_set_correctly(self):
        first_repository, *_ = self.get_repositories()

        self.assertEqual(first_repository.name, 'grit')
        self.assertEqual(
            first_repository.description,
            '**Grit is no longer maintained. '
            'Check out libgit2/rugged.** Grit gives you object oriented read/write access to Git repositories via Ruby.'
        )
        self.assertEqual(first_repository.link, 'https://github.com/mojombo/grit')
        self.assertEqual(first_repository.clone_url, 'https://github.com/mojombo/grit.git')
        self.assertEqual(first_repository.author, '')
        self.assertEqual(first_repository.author_username, 'mojombo')

    def test_that_github_api_is_called_correctly(self):
        self.get_repositories()

        self.mock_requests.get.assert_called_once_with('https://api.github.com/repositories', params={})

    def test_that_APIError_is_raised_when_github_api_returns_unexpected_status_code(self):
        self.mock_requests.get.return_value = StubResponse(b'ERROR', status_code=500)

        with self.assertRaises(APIError) as error:
            self.get_repositories()

        self.assertEqual(str(error.exception), 'Communication Error: 500 Server Error: None for url: None')

    def test_that_APIError_is_raised_when_github_api_returns_no_json_formatted_content(self):
        self.mock_requests.get.return_value = StubResponse(b'plaindata', status_code=200)

        with self.assertRaises(APIError) as error:
            self.get_repositories()

        self.assertEqual(str(error.exception), 'Communication Error: No valid JSON content returned')

    def test_that_github_api_is_called_correctly_when_search_parameter_given(self):
        self.mock_requests.get.return_value = self.get_github_search_fixture_response()
        self.get_repositories(search='searchterm')

        self.mock_requests.get.assert_called_once_with(
            'https://api.github.com/search/repositories',
            params={'q': 'searchterm'}
        )

    def test_that_search_return_value_is_list_of_repository_responses(self):
        self.mock_requests.get.return_value = self.get_github_search_fixture_response()
        repositories = self.get_repositories(search="searchterm")
        all_repository_objects = (isinstance(repository, Repository) for repository in repositories)
        self.assertTrue(all(all_repository_objects))
