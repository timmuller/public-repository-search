from search_api.git_integration.repository import get_repositories, Repository, APIError
from search_api.tests.git_integration.testcase import StubResponse, IntegrationTestCase


class TestGitlabIntegration(IntegrationTestCase):
    def setUp(self):
        self.mock_requests = self.setup_mock('search_api.git_integration.repository.requests')
        self.mock_requests.get.return_value = self.get_gitlab_fixture_response()

    def get_gitlab_fixture_response(self):
        return self.get_fixture_response('gitlab_projects_response.json')

    def test_that_20_repositories_are_returned_from_fixture(self):
        repositories = get_repositories()
        self.assertEqual(len(repositories), 20)

    def test_that_return_value_is_list_of_repository_responses(self):
        repositories = get_repositories()
        all_repository_objects = (isinstance(repository, Repository) for repository in repositories)
        self.assertTrue(all(all_repository_objects))

    def test_that_first_respository_contains_correct_data_based_on_fixture(self):
        first_repository, *_ = get_repositories()
        self.assertEqual(first_repository.name, "WPI WebWork Status")
        self.assertEqual(first_repository.description, "Front-end/back-end combo for WPI WebWork Status")
        self.assertEqual(first_repository.link, "https://gitlab.com/o355/wpiwebworkstatus")
        self.assertEqual(first_repository.clone_url, "git@gitlab.com:o355/wpiwebworkstatus.git")
        self.assertEqual(first_repository.author, "Owen McGinley")
        self.assertEqual(first_repository.author_username, "o355")

    def test_that_gitlab_api_is_called(self):
        get_repositories()

        self.mock_requests.get.assert_called_once_with('https://gitlab.com/api/v4/projects', params={})

    def test_that_search_parameter_is_passed_as_querystring_to_gitlab(self):
        get_repositories(search="searchterm")

        self.mock_requests.get.assert_called_once_with(
            'https://gitlab.com/api/v4/projects',
            params={'search': 'searchterm'}
        )

    def test_that_APIError_is_raised_when_gitlab_api_returns_unexpected_status_code(self):
        self.mock_requests.get.return_value = StubResponse(b'ERROR', status_code=500)

        with self.assertRaises(APIError) as error:
            get_repositories()

        self.assertEqual(str(error.exception), 'Communication Error: 500 Server Error: None for url: None')

    def test_that_APIError_is_raised_when_gitlab_api_returns_no_json_formatted_content(self):
        self.mock_requests.get.return_value = StubResponse(b'plaindata', status_code=200)

        with self.assertRaises(APIError) as error:
            get_repositories()

        self.assertEqual(str(error.exception), 'Communication Error: No valid JSON content returned')
