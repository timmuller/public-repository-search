from search_api.git_integration.repository import get_repositories, APIError, get_provider, GitLabProvider
from search_api.tests.testcase import TestCase


class TestIncorrectProvider(TestCase):
    def test_that_APIError_is_returned_when_no_existing_integration_is_used(self):
        with self.assertRaises(APIError) as error:
            get_repositories(provider='doesnotexist')
        self.assertEqual(str(error.exception), 'Provider doesnotexist does not exist')


class TestGetProvider(TestCase):
    def test_that_no_provider_selection_will_fallback_on_gitlab(self):
        provider_class = get_provider(provider=None)
        self.assertEqual(provider_class, GitLabProvider)
