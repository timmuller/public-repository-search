import os
import pathlib
from requests import Response
from search_api.tests.testcase import TestCase


class StubResponse(Response):
    def __init__(self, content, status_code=200):
        super(StubResponse, self).__init__()
        self.status_code = status_code
        self._content = content


class IntegrationTestCase(TestCase):
    def get_fixture_response(self, filename):
        fixture_path = os.path.join(pathlib.Path(__file__).parent.absolute(), filename)
        with open(fixture_path, 'rb') as fh:
            return StubResponse(fh.read(), status_code=200)
