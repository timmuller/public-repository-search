from search_api.serializers import RepositorySerializer
from search_api.tests.factories import RepositoryFactory
from search_api.tests.testcase import TestCase


class TestRepositorySerializer(TestCase):

    def test_repository_object_can_be_serialized(self):
        repository = RepositoryFactory.create(
            name='name',
            description='description',
            link='link',
            clone_url='ssh_clone',
            author='Author',
            author_username='author_username'
        )

        serialized_data = RepositorySerializer(repository).data
        self.assertEqual(serialized_data, {
            'name': 'name',
            'description': 'description',
            'link': 'link',
            'clone_url': 'ssh_clone',
            'author': 'Author',
            'username': 'author_username'
        })
