from unittest import mock
from django.test import TestCase


class TestCase(TestCase):
    def setup_mock(self, target):
        target_mock_wrapper = mock.patch(target)
        target_mock = target_mock_wrapper.start()
        self.addCleanup(target_mock.stop)
        return target_mock
