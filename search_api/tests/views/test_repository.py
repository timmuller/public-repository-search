from django.urls import reverse

from search_api.git_integration.repository import APIError
from search_api.serializers import RepositorySerializer
from search_api.tests.factories import RepositoryFactory
from search_api.tests.testcase import TestCase


class TestRepositoryViewSet(TestCase):
    def setUp(self):
        self.url = reverse('repository-list')
        self.mock_repositories = self.setup_mock('search_api.views.get_repositories')
        self.mock_repositories.return_value = []

    def test_that_url_list_endpoint_can_be_found(self):
        self.assertEqual(self.url, '/api/repository/')

    def test_that_empty_list_is_returned_when_no_repositories_returned(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_that_one_repository_is_returned(self):
        repository = RepositoryFactory.create()
        self.mock_repositories.return_value = [repository]

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [
            RepositorySerializer(repository).data
        ])

    def test_that_multiple_repositories_are_returned(self):
        repository1, repository2, repository3 = RepositoryFactory.create_batch(3)
        self.mock_repositories.return_value = [repository1, repository2, repository3]

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [
            RepositorySerializer(repository1).data,
            RepositorySerializer(repository2).data,
            RepositorySerializer(repository3).data,
        ])

    def test_that_503_is_returned_when_interacting_with_git_services_failed(self):
        self.mock_repositories.side_effect = APIError("error")

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 503)
        self.assertEqual(response.content, b'{"detail":"error"}')

    def test_that_get_param_search_is_used_to_search_repositories_with(self):
        repository1, repository2, repository3 = RepositoryFactory.create_batch(3)
        self.mock_repositories.return_value = [repository1, repository2, repository3]

        self.client.get(self.url, {'search': 'searchterm'})

        self.mock_repositories.assert_called_once_with(provider=None, search='searchterm')

    def test_that_querystring_github_is_passed(self):
        repository1, repository2, repository3 = RepositoryFactory.create_batch(3)
        self.mock_repositories.return_value = [repository1, repository2, repository3]

        self.client.get(self.url, {'provider': 'github', 'search': 'searchterm'})

        self.mock_repositories.assert_called_once_with(provider='github', search='searchterm')
