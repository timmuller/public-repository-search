from django.urls import path, include
from rest_framework import routers
from search_api import views

router = routers.DefaultRouter()
router.register(r'repository', views.RepostoryViewSet, basename="repository")
urlpatterns = [
    path('', include(router.urls)),
]
