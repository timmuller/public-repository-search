from rest_framework import mixins
from rest_framework.exceptions import APIException
from rest_framework.viewsets import GenericViewSet

from search_api.git_integration.repository import get_repositories, APIError
from search_api.serializers import RepositorySerializer


class ServiceUnavailable(APIException):
    status_code = 503
    default_detail = 'Service temporarily unavailable, try again later.'
    default_code = 'service_unavailable'


class RepostoryViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = RepositorySerializer

    def get_queryset(self):
        search_term = self.request.query_params.get('search')
        provider = self.request.query_params.get('provider')
        try:
            return get_repositories(provider=provider, search=search_term)
        except APIError as e:
            raise ServiceUnavailable(e)
